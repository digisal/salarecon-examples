clc; clear; close all;

%% BLAST proteomes

prot = 'data/proteomes/salmo_salar_mapped.fasta';
model_ids = {'danio_rerio'};

ref_prot = cell(1,length(model_ids));
for i=1:length(model_ids)
    ref_prot{i} = strcat('data/proteomes/',model_ids{i}, ...
        '_mapped_model.fasta');
end

blast = getBlast('salmo_salar',prot,model_ids,ref_prot);

%% Load template models

dr = importModel('models/ZebraGEM_2_0.xml');
dr.id = 'danio_rerio';

models = {dr};

%% Build salmon model from homology

ss = getModelFromHomology(models,blast,'salmo_salar');


%% Fix compartment IDs

ss.comps = {'vesicule','lysosomalmembrane','mitochondrialmembrane','l','c','endoplasmicreticulummembrane','m','r','mem','g','environment','golgimembrane','peroxisomalmembrane','n','mitochondrialoutermembrane','p','i','mim','plasmamembrane','e','nuclearmembrane'};

%% Add biomass reaction

new.rxns = {'Biomass'};
new.equations = {'accoa_c + ala__L_c + arg__L_c + asn__L_c + asp__L_c + atp_c + chsterol_c + clpn_mem + ctp_c + cys__L_c + dag_c + datp_c + dctp_c + dgtp_c + dttp_c + gln__L_c + glu__L_c + gly_c + gtp_c + h_c + his__L_c + ile__L_c + leu__L_c + lpchol_mem + lys__L_c + mag_c + met__L_c + o2_c + pa_c + pail_mem + pchol_c + pe_c + pglyc_c + phe__L_c + pro__L_c + prpp_c + ps_mem + ser__L_c + sphmyln_mem + tag_c + thr__L_c + trp__L_c + tyr__L_c + utp_c + val__L_c + xolest_hs_mem => Cell + adp_c + amp_c + coa_c + gdp_c + h2o_c + pi_c + ppi_c'};
new.rxnNames = new.rxns;
new.lb = [0];
new.ub = [1000];
new.c = [1];

addRxns(ss,new,1,'c',true,false);

%% Fill gaps

myge_ens = importModel('models/M_genitalium_G37_ensemble.xml');

mysa_final = fillGaps(mysa,{myge_ens},false,false);
