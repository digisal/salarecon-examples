#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cobra
import sys


def map_fasta(fasta_file, map_file, model_file):
    """Maps IDs and writes new FASTA file with genes found in model."""

    # Load ID map
    id_map = load_map(map_file)

    # Load model and get genes
    model = cobra.io.read_sbml_model(model_file)
    model_genes = set(g.id.split('_')[0] for g in model.genes)

    # Convert FASTA file
    new_lines = []
    mapped = False
    with open(fasta_file, 'r') as f:
        for line in f:
            if line.startswith('>'):
                old_id = line.split('|')[1]
                try:
                    new_id = id_map[old_id]
                except KeyError:
                    mapped = False
                    continue
                if new_id not in model_genes:
                    mapped = False
                    continue
                else:
                    new_lines.append('>' + new_id)
                    mapped = True
            elif mapped:
                new_lines.append(line.strip())

    # Write to new FASTA file
    new_file = fasta_file.split('.fasta')[0] + '_mapped_model.fasta'
    with open(new_file, 'w') as f:
        f.write('\n'.join(new_lines) + '\n')


def load_map(map_file):
    """Loads ID map from file."""
    with open(map_file, 'r') as f:
        f.readline()
        return dict(line.strip().split('\t') for line in f)


if __name__ == '__main__':
    fasta_file = sys.argv[1]
    map_file = sys.argv[2]
    model_file = sys.argv[3]
    map_fasta(fasta_file, map_file, model_file)
