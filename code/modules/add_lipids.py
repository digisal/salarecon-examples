"""
Functions for adding lipid metabolism to SALARECON
"""

import pandas as pd
from cobra.core import Reaction, Metabolite
from collections import Counter


data_path = "../../data/lipid_metabolites/"
lipids = pd.read_csv(data_path + "lipid_metabolites.csv", index_col=0).fillna("")
reactions = pd.read_csv(data_path + "lipid_reactions.csv").set_index("type")


def fas1_reactions():
    """
    Return list of reaction strings for FAS1 reactions

    EC 2.3.1.85
    """
    fa_list = "but hxa octa dca ddca ttdca hdca".split(" ")
    r_list = list()
    for fa1, fa2 in zip(fa_list[:-1], fa_list[1:]):
        reactants = f"{fa1}_c + malcoa_c + 2 nadph_c + 2 h_c"
        products = f"{fa2}_c + coa_c + co2_c + 2 nadp_c"
        r_list.append(f"{reactants} --> {products}")
    return r_list


def add_fas1_reactions(model):
    """
    Add reaction for shorter (<16C) fatty acid synthesis.
    """
    for n, r_str in zip(range(3, 8), fas1_reactions()):
        r = Reaction(id=f"FAS{2 * n}0COA")
        model.add_reactions([r])
        r.reaction = r_str
        r.annotation["ec-code"] = "2.3.1.85"
        r.annotation["kegg.reaction"] = "R05188"
        r.gene_reaction_rule = " or ".join("106589612 106610271".split(" "))


def add_fatty_acid_metabolites(model, metabolite_reference=lipids):
    """
    Add metabolites for cytosol(c), mitochondrion and ER (r)
    """
    for l_n, data in metabolite_reference.iterrows():
        for compartment in "cmr":
            coamet = Metabolite(f"{get_fatty_acyl_coa_id(l_n)}_{compartment}",
                                elements_to_formula(FA_coa_elements(l_n)),
                                name=data["name_coa"],
                                compartment=compartment)
            met = Metabolite(f"{get_free_fatty_acyl_id(l_n)}_{compartment}",
                             elements_to_formula(FA_elements(l_n)),
                             name=data["name"],
                             compartment=compartment)
            model.add_metabolites([coamet, met])
            if data["kegg_coa"]:
                coamet.annotation["kegg.compound"] = data["kegg_coa"]


def infer_metabolite_formulae_from_reaction_balance(model):
    """
    Use elemental balance between remaining metabolites to balance reactions elementally.

    This only works for reactions with one missing elemental formula,
    and should not be performed on transport reactions
    >>> from cobra.core import Model
    >>> model = Model("test_model")
    >>> r = Reaction("test_rxn")
    >>> met = Metabolite("a", compartment="c")
    >>> met.formula = "CH2O"
    >>> model.add_metabolites([met])
    >>> model.add_reactions([r])
    >>> r.reaction = "a --> b"
    >>> model.metabolites.b.compartment = "c"
    >>> infer_metabolite_formulae_from_reaction_balance(model)
    >>> model.metabolites.b.formula
    "CH2O"
    """
    keep_going = True
    while keep_going:
        old_len = len([m for m in model.metabolites if not m.formula])
        for r in model.reactions:
            coefs = r.metabolites.values()
            if sum(coefs) % 1 or len(coefs) == 1 or len(r.compartments) > 1:
                continue
            na_mets = [m for m in r.metabolites.keys() if not m.formula]
            if len(na_mets) == 1:
                met = na_mets.pop()
                c = Counter()
                for key, val in r.metabolites.items():
                    md = {e: v * val for e, v in key.elements.items()}
                    c.update(Counter(md))
                met.formula = elements_to_formula(c)
        new_len = len([m for m in model.metabolites if not m.formula])
        keep_going = old_len != new_len


def get_elemental_composition_from_siblings(model):
    """
    Get elemental composition from identical metabolites in other compartments

    Assuming elemental composition to be the same if it is not specified,
    and ids to be the same except last character.
    """
    for m in model.metabolites:
        if not m.formula:
            for met in model.metabolites:
                if met.id.startswith(m.id[:-1]):
                    if met.formula:
                        m.formula = met.formula


def fatty_acyl_coa_hydrolase(lipid_number, compartment):
    """
    Return FA-CoA hydrolase reaction string
    """
    facoa_id = get_fatty_acyl_coa_id(lipid_number)
    fa_id = get_free_fatty_acyl_id(lipid_number)
    c = compartment
    return f"{facoa_id}_{c} + h2o_{c} <=> {fa_id}_{c} + coa_{c} + h_{c}"


def deconstruct_lipid_number(lipid_number):
    """
    Return dict given lipid number

    >>> deconstruct_lipid_number('C18:1 n3')
    {'length': 18, 'double_bonds': 1, 'omega': 3}
    """
    if not lipid_number.startswith('C'):
        return
    fa_dict = dict()
    fa_dict["length"], rest = lipid_number[1:].split(':')
    if "n" in rest:
        fa_dict["double_bonds"], fa_dict["omega"] = rest.split(' n')
    else:
        fa_dict["double_bonds"] = rest
    return {key: int(val) for key, val in fa_dict.items()}


def reconstruct_lipid_number(fa_dict):
    """
    Return fatty acid code given dict

    >>> d = {'length': 18, 'double_bonds': 1, 'omega': 3}
    >>> reconstruct_lipid_number(d)
    'C18:1 n3'

    """
    l, db, omega = fa_dict["length"], fa_dict["double_bonds"], fa_dict.get("omega")
    s = f"C{l}:{db}"
    if omega:
        s += f" n{omega}"
    return s


def fatty_acid_product(fatty_acid, reaction_type):
    """"
    Determine the product of reaction given substrate

    >>> fatty_acid_product('C16:1 n3', 'elongation')
    'C18:1 n3'
    >>> fatty_acid_product('C16:1 n3', 'delta6')
    'C18:2 n3'
    >>> fatty_acid_product('C16:0', 'delta9')
    'C16:1 n7'
    """
    fa_dict = deconstruct_lipid_number(fatty_acid)
    if not fa_dict:
        return
    if reaction_type == "elongation":
        fa_dict["length"] += 2
    if reaction_type == "delta6":
        if not fa_dict.get("omega"):
            fa_dict["omega"] = fa_dict["length"] - 6
        fa_dict["double_bonds"] += 1
    if reaction_type == "delta9":
        if not fa_dict.get("omega"):
            fa_dict["omega"] = fa_dict["length"] - 9
        fa_dict["double_bonds"] += 1
    if reaction_type == "delta5":
        if not fa_dict.get("omega"):
            fa_dict["omega"] = fa_dict["length"] - 5
        fa_dict["double_bonds"] += 1
    if reaction_type == "beta_oxidation":
        fa_dict["length"] -= 2
    return reconstruct_lipid_number(fa_dict)


def add_elongation_mid_product(model, product, step, compartment):
    change = {0: {"H": -2, "O": 1}, 1: {"O": 1}, 2: {"H": -2}}
    met_id = get_elongation_mid_product_ids(get_fatty_acyl_coa_id(product))[step]
    formula = elements_to_formula(dict(Counter(FA_coa_elements(product)) +
                                       Counter(change[step])))
    model.add_metabolites([Metabolite(f"{met_id}_{compartment}", formula)])


def get_elongation_mid_product_ids(product):
    return [f"3o{product}", f"3h{product}", product.replace("coa", "2coa")]


def add_fael(model, fatty_acid, product, step, ref, compartment="r"):
    """
    Add fatty acid elongation reactions to model
    """
    fa_id, p_id = (get_fatty_acyl_coa_id(s) for s in (fatty_acid, product))
    if step < 3:
        add_elongation_mid_product(model, product, step, compartment)
    r = Reaction(id=f"FAEL{step}{fa_id}{compartment}")
    model.add_reactions([r])
    mets = [fa_id] + get_elongation_mid_product_ids(p_id) + [p_id]
    start, prod = mets[step:step + 2]
    r.reaction = lipid_reaction_str(start, prod, compartment, ref)


def add_fatty_acid_reactions(model,
                             metabolite_reference=lipids,
                             r_ref=reactions):
    """
    Add fatty acid (FA) reactions to model by iterating over reaction types and FAs.

    """
    for lipid in list(metabolite_reference.index):
        for reaction_type in ["elongation"] + [f"delta{i}" for i in [5, 6, 9]]:
            add_fatty_acid_reaction(model=model, fatty_acid=lipid, r_type=reaction_type,
                                    compartment="r",
                                    metabolite_reference=metabolite_reference,
                                    r_ref=r_ref)


def add_fatty_acid_reaction(model, fatty_acid, r_type,
                            compartment="r",
                            metabolite_reference=lipids,
                            r_ref=reactions):
    """
    Add a specified fatty acid reaction to model
    """
    product = fatty_acid_product(fatty_acid, r_type)
    if product not in metabolite_reference.index:
        return
    if r_type == "elongation":
        for step in range(4):
            rxn_ref = r_ref.loc[f"elongation{step}"]
            add_fael(model, fatty_acid, product, step, rxn_ref, compartment=compartment)
    else:
        fa_id, p_id = (get_fatty_acyl_coa_id(fa) for fa in (fatty_acid, product))
        rxn_ref = r_ref.loc[r_type]
        rxn = Reaction(id=rxn_ref.id + fa_id + compartment)
        model.add_reactions([rxn])
        rxn.reaction = lipid_reaction_str(fa_id, p_id, compartment, rxn_ref)


def get_fatty_acyl_coa_id(fatty_acid, metabolite_reference=lipids):
    m_ref = metabolite_reference.loc[fatty_acid]
    metcoa_id, met_id = m_ref.get("id_coa", False), m_ref.get("id", False)
    if metcoa_id:
        return metcoa_id
    if met_id:
        return f"{met_id}coa"
    return fatty_acid.replace(":", "").replace(" ", "") + "coa"


def get_free_fatty_acyl_id(fatty_acid, metabolite_reference=lipids):
    m_ref = metabolite_reference.loc[fatty_acid]
    met_id = m_ref.get("id", False)
    return met_id if met_id else fatty_acid.replace(":", "").replace(" ", "")


def lipid_reaction_str(start, product, comp="r", ref=None):
    co, by = ref[["cofactors", "byproducts"]].fillna("").str.strip()
    r = list()
    for side, main in zip([co, by], [start, product]):
        if side:
            mets = [f"{m}_{comp}" for m in [main] + list(side.split(" + "))]
            r.append(" + ".join(mets))
        else:
            r.append(f"{main}_{comp}")
    return " --> ".join(r)


def FA_coa_elements(C_str: str):
    coa = {'C': 21, 'H': 31, 'N': 7, 'O': 15,  'P': 3, 'S': 1}
    fa = FA_elements(C_str)
    return dict(Counter(coa) + Counter(fa))


def FA_elements(C_str: str):
    d = deconstruct_lipid_number(C_str)
    formula = {'C': d["length"],
               'H':  2 * d["length"] - 1 - 2 * d["double_bonds"],
               'O': 2}
    return formula


def elements_to_formula(formula_dict):
    return "".join([f"{k}{abs(int(v))}" if abs(int(v)) > 1 else f"{k}"
                    for k, v in formula_dict.items()])
