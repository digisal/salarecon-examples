# -*- coding: utf-8 -*-

"""
Functions for analysis and visualisation of response to feeds
with different amino acid compositions and the effect of amino
acid supplements
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import cobra
import seaborn as sns
import re
from matplotlib.ticker import MultipleLocator

feed_comp = pd.read_csv("../../data/feed_AA_composition.csv", index_col="AA")
feed_comp = 1000 * feed_comp / feed_comp.sum()
feed_comp.T[["gln/glu", "asn/asp"]] = feed_comp.T[["gln/glu", "asn/asp"]] / 2
aa_colors = pd.read_csv("../../data/AA_colors.csv", index_col="AA")
col_hex = aa_colors.hex
col_hex["feed\n  protein"] = "#999999"


def gram_to_mol(model, met_id, mass):
    """
    Convert amount in grams of metabolite to amount in mol of metabolite

        Parameters:
            model (cobra.core.Model): metabolic model
            met_id (str): name of metabolite
            mass (float): amount of metabolite in grams

        Returns:
            float: amount of metabolite in mol
    >>> import cobra.test
    >>> test = cobra.test.create_test_model('textbook')
    >>> gram_to_mol(test, '2pg_c', 1)
    0.005463483087058729
    """
    return mass / model.metabolites.get_by_id(met_id).formula_weight


def AA_isomer(aa):
    """
    Convert AA-name to L-form if not glycine

        Parameters:
            aa (str): amino acid name

        Returns:
            str: amino acid name with isomerization (bigg id)

    >>> AA_isomer('gly')
    'gly'
    >>> AA_isomer('ala')
    'ala__L'
    """
    if aa == "gly":
        return aa
    else:
        return aa + "__L"


def AA_to_EX(aa_str):
    """
    Convert amino acid names to exchange reactions

        Parameters:
            aa_str (str): amino acid name

        Returns:
            str: amino acid exchange reaction id

    >>> AA_to_EX('ala')
    'EX_ala__L_e'
    """
    return "EX_{}_e".format(AA_isomer(aa_str))


def AA_table_to_EX(table=feed_comp):
    """
    Convert a table of amino acids to a list of BiGG exchange reaction ids

        Parameters:
            table (pandas.DataFrame): table with metabolites as row names

        Returns:
            list: reaction ids in format of exchange reactions

    >>> feed = pd.Series({'ala': 1, gly: 1})
    >>> AA_table_to_EX(feed)
    ['EX_ala__L_e', 'EX_gly_e']
    """
    reactions = list()
    for aa in table.index:
        if "/" in aa:
            reactions += aa.split("/")
        else:
            reactions.append(aa)
    return [AA_to_EX(a) for a in reactions]


def AA_table_to_rev(table=feed_comp):
    """
    Returns a table of amino acids as reversed exchange reaction ids.

        Parameters:
            table (pandas.DataFrame): table with metabolites as row names

        Returns:
            list: reaction ids in format of reversed reactions

    >>> feed = pd.Series({'ala': 1, gly: 1})
    >>> AA_table_to_rev(feed)
    ['EX_ala__L_e_rev', 'EX_gly_e_rev']
    """
    return [a + "_rev" for a in AA_table_to_EX(table=feed_comp)]


def define_AA_media(model, medium):
    """
    Define model with provided AA composition.

        Parameters:
            model (cobra.core.Model): metabolic model
            medium (pandas.Series): Amino acid names and corresponding mass
                                    proportions

    >>> import cobra.test
    >>> test = cobra.test.create_test_model('textbook')
    >>> feed = pd.Series({'glu': 1})
    >>> define_AA_media(test, feed)
    >>> test.medium.get('EX_glu__L_e')
    0.0068436282946253155
    """
    for aa, val in medium.iteritems():
        if "/" in aa:
            aa_list = aa.split("/")
        else:
            aa_list = [
                aa,
            ]
        for a in aa_list:
            a = AA_isomer(a) + "_e"
            add_boundary(model, a, gram_to_mol(model, a, val))


def is_import(rxn):
    """
    Returns 1*import direction for import reactions, otherwise 0

        Parameters:
            rxn (cobra.core.Reaction)

        Returns:
            -1 or 1 or 0: direction of import (0 means it cannot import)

    >>> import cobra.test
    >>> test = cobra.test.create_test_model('textbook')
    >>> is_import(test.reactions.ACALD)
    0
    >>> is_import(test.reactions.EX_glu__L_e)
    1
    >>> test.reactions.EX_glu__L_e.upper_bound = 0.0
    >>> is_import(test.reactions.EX_glu__L_e)
    0
    """
    if len(rxn.metabolites) == 1 and rxn.compartments == {"e"}:
        makes_products = bool(rxn.products and rxn.upper_bound)
        makes_reactants = bool(rxn.reactants and rxn.lower_bound)
        return makes_products - makes_reactants
    else:
        return 0


def is_export(rxn):
    """
    Returns export direction

        Parameters:
            rxn (cobra.core.Reaction)
    Returns:
        -1 or 1 or 0: direction of export (0 means it cannot export)


    >>> import cobra.test
    >>> test = cobra.test.create_test_model('textbook')
    >>> is_export(test.reactions.ACALD)
    0
    >>> is_export(test.reactions.EX_glu__L_e)
    -1
    >>> test.reactions.EX_glu__L_e.lower_bound = 0.0
    >>> is_export(test.reactions.EX_glu__L_e)
    0
    """
    if len(rxn.metabolites) == 1 and rxn.compartments == {"e"}:
        exports_reactants = bool(rxn.reactants and rxn.upper_bound)
        exports_products = bool(rxn.products and rxn.lower_bound)
        return exports_reactants - exports_products
    else:
        return 0


def disable_import(rxn):
    """
    Disable import but not export

        Parameters:
            rxn (cobra.core.Reaction)
    >>> import cobra.test
    >>> test = cobra.test.create_test_model('textbook')
    >>> disable_import(test.reactions.EX_co2_e)
    >>> test.reactions.EX_co2_e.lower_bound
    0.0
    >>> disable_import(test.reactions.ACALD)
    >>> test.reactions.ACALD.lower_bound
    -1000.0
    """
    if is_import(rxn):
        pass
    else:
        return
    if rxn.reactants and rxn.lower_bound:
        rxn.lower_bound = 0.0
    if rxn.products and rxn.upper_bound:
        rxn.upper_bound = 0.0


def disable_export(rxn):
    """
    Disable export but not import.
    To avoid export of previously limiting AA

        Parameters:
            rxn (cobra.core.Reaction)
    >>> import cobra.test
    >>> test = cobra.test.create_test_model('textbook')
    >>> disable_import(test.reactions.EX_co2_e)
    >>> test.reactions.EX_co2_e.lower_bound
    0.0
    >>> disable_import(test.reactions.ACALD)
    >>> test.reactions.ACALD.lower_bound
    -1000.0
    """
    if is_import(rxn):
        pass
    else:
        return
    if rxn.reactants and rxn.upper_bound:
        rxn.upper_bound = 0.0
    if rxn.products and rxn.lower_bound:
        rxn.lower_bound = 0.0


def enable_import(rxn, rate=1000):
    """
    Enable import for exchange reaction

        Parameters:
            rxn (cobra.core.Reaction): reaction
            rate (float): maximum import rate
    >>> test = cobra.test.create_test_model('textbook')
    >>> enable_import(test.reactions.EX_ac_e)
    >>> test.reactions.EX_ac_e.lower_bound
    -1000.0
    """
    if rxn.compartments == {"e"} and len(rxn.metabolites) == 1:
        pass
    else:
        return
    if rxn.reactants:
        rxn.lower_bound = -rate
    if rxn.products:
        rxn.upper_bound = rate


def increase_import(rxn, step):
    """
    Increase the allowed import rate

        Parameters:
            rxn (cobra.core.Reaction): reaction
            step (float): increment

    >>> test = cobra.test.create_test_model('textbook')
    >>> increase_import(test.reactions.EX_ac_e, 1)
    >>> test.reactions.EX_ac_e.lower_bound
    -1.0
    """
    if rxn.reactants:
        rxn.lower_bound -= step
    elif rxn.products:
        rxn.upper_bound += step


def add_boundary(model, met_id, bound):
    """
    Set bound for maximum access to given metabolite

        Parameters:
            model (cobra.core.Model): metabolic model
            met_id (str): id of extracellular metabolite
            bound (float): maximal import rate
    """
    try:
        rxn = model.exchanges.get_by_id("EX_" + met_id + "_rev")
    except KeyError:
        rxn = model.exchanges.get_by_id("EX_" + met_id)
    finally:
        enable_import(rxn, rate=bound)


def lim_met(model, sol, pfba=False, only_aa=True, by_mass=True):
    """
    Return exchange reaction with the lowest reduced cost

        Parameters:
            model (cobra.core.Model): metabolic model
            sol (cobra.core.solution): Result of fba, pfba, etc

        Returns:
            lim (cobra.core.Reaction): the limiting exchange reaction

    >>> import cobra.test
    >>> test = cobra.test.create_test_model('textbook')
    >>> lim_met(test, test.optimize())
    'EX_fru_e'
    """
    if only_aa:
        exlist = AA_table_to_EX()
    else:
        exlist = [e.id for e in model.exchanges]
    if by_mass:
        rc = pd.Series(
            {
                ex: sol.reduced_costs[ex]
                / model.metabolites.get_by_id(ex[3:]).formula_weight
                for ex in exlist
            }
        )
    else:
        rc = sol.reduced_costs[exlist]
    if pfba:
        lim = rc.index[np.argmax(rc)]
    else:
        lim = rc.index[np.argmin(rc)]
    return lim


def lim_aa(sol):
    """
    Return the limiting amino acid

        Parameters:
            sol (cobra.core.solution): Result of fba, pfba, etc
    """
    aa_cost = sol.reduced_costs[AA_table_to_EX()]
    lim = aa_cost.index[np.argmin(aa_cost)]
    return lim


def plot_medium(model, text="", **kwargs):
    """
    Visualize medium composition for model

        Parameters:
            model (cobra.core.Model): metabolic model
            text (str): description of medium
    """
    idx = [idx[3:] for idx in model.medium.keys()]
    header = "Medium composition " + text
    medium = pd.DataFrame(model.medium.values(), index=idx)
    medium.plot.bar(legend=False, figsize=(20, 2), title=header, **kwargs).set_yscale(
        "log"
    )


def plot_reduced_costs(sol, **kwargs):
    """
    Plot reduced costs of amino acids in cobra solution

        Parameters:
            sol (cobra.core.solution): Result of fba, pfba, etc
    """
    header = "How important is the import of amino acid X for the objective?"
    try:
        aa_rc = sol.reduced_costs[[e for e in AA_table_to_rev()]]
    except KeyError:
        aa_rc = sol.reduced_costs[[e for e in AA_table_to_EX()]]
    finally:
        aa_rc.plot.bar(
            legend=False,
            figsize=(20, 2),
            title=header,
            ylabel="reduced costs",
            **kwargs
        )


def plot_shadow_price(sol, **kwargs):
    """
    Plot shadow prices of amino acids in cobra solution

        Parameters:
            sol (cobra.core.solution): Result of fba, pfba, etc
    """
    aa_list = [a[3:] for a in AA_table_to_EX()]
    hd = "How much does the objective change by the addition of amino acid X?"
    sol.shadow_prices[aa_list].plot.bar(
        legend=False, figsize=(20, 2), title=hd, ylabel="Shadow price", **kwargs
    )


def top_limiting(model, n=10, step=0.1):
    """
    Identify the top n limiting metabolites in the medium

        Parameters:
            model (cobra.core.Model): a metabolic model
            n (int): number of iterations
            step (float): allowed increase in flux

        Returns:
            toplim (list): the top n+1 limiting exchange reactions
    """
    toplim = list()
    with model as mod:
        for _ in range(n):
            msol = mod.optimize()
            ex = lim_met(mod, msol)
            rxn = model.exchanges.get_by_id(ex)
            increase_import(rxn=rxn, step=step)
            toplim.append(rxn)
        return toplim


def simulation_df(model, n):
    """
    Iterate over limiting metabolites.

    For n iterations, optimize, identify limiting metabolite and
    loosen the constraint

        Parameters:
            model (cobra.core.Model): a metabolic model
            n (int): number of iterations

        Returns:
            flux_df (pandas.DataFrame): flux data for each optimization
            rc_df (pandas.DataFrame): reduced costs for each optimization
    """
    sol = model.optimize()
    lim = lim_met(model, sol)
    flux_df = pd.DataFrame({str(lim)[3:].split("_rev")[0]: sol.fluxes})
    rc_df = pd.DataFrame({str(lim)[3:].split("_rev")[0]: sol.reduced_costs})
    with model as mod:
        for _ in range(n):
            rxn = model.exchanges.get_by_id(lim)
            increase_import(rxn=rxn, step=10)
            sol = mod.optimize()
            lim = lim_met(mod, sol)
            flux_df[str(lim)[3:].split("_rev")[0]] = sol.fluxes
            rc_df[str(lim)[3:].split("_rev")[0]] = sol.reduced_costs
    return flux_df, rc_df


def plot_limiting(model, n):
    """
    Plot fluxes and reduced costs of amino acids for n iterations
    Loosening the constraint for the limiting metabolite between iterations

        Parameters:
            model (cobra.core.Model): a metabolic model
            n (int): number of iterations
    """
    fluxes, red_costs = simulation_df(model, n)
    BM0 = fluxes.iloc[:, 0].loc["Biomass"]
    _, axes = plt.subplots(n, figsize=[5, 20])
    for (fluxid, flx), (_, rc), ax in zip(
        fluxes.T.iterrows(), red_costs.T.iterrows(), axes
    ):
        BMi = flx["Biomass"]
        improvement = 100 * (BMi - BM0) / BM0
        aa_import = flx[AA_table_to_rev()]
        ax.pie(aa_import / sum(aa_import.values), normalize=True)
        rc_aa = rc[AA_table_to_rev()]
        lim_aa = rc_aa.index[np.argmax(rc_aa)]
        title = "growth: {:0.2f} improvement: {:0.2f}% limiting: {}"
        ax.title.set_text(title.format(BMi, improvement, lim_aa + " " + fluxid))
    plt.show()
    sns.heatmap(fluxes.loc[AA_table_to_rev()])
    plt.title("Fluxes")
    plt.xlabel("limiting amino acid")
    plt.show()
    plt.figure(figsize=(4, 6))
    ax0 = plt.subplot2grid((4, 1), (0, 0), rowspan=1)
    bm_improvement = fluxes.T.Biomass / fluxes.T.Biomass[0]
    bm_improvement.plot(kind="bar", ax=ax0, width=0.5, title="Biomass improvement")
    plt.xticks([])
    ax1 = plt.subplot2grid((4, 1), (1, 0), rowspan=5)
    sns.heatmap(
        red_costs.loc[
            AA_table_to_rev(),
        ],
        cbar=False,
        ax=ax1,
    )
    plt.xlabel("limiting amino acid")
    plt.ylabel("Reduced cost of amino acids")


def simulation_list(
    model,
    n,
    feed=feed_comp.SBM,
    enable_carbon=True,
    step_size=10000,
    pfba=False,
    aa_cost=1,
    lim_by_mass=True,
):
    """
    Iterate over limiting metabolites using pfba.

        Parameters:
            model (cobra.core.Model): a metabolic model
            n (int): number of iterations
            feed (pd.Series): Amino acid composition
            enable_carbon (bool):
            step_size (float): allowed increment of limiting metabolite

        Returns:
            sols (list): list of cobra.core.solution
    """
    for e in model.exchanges:

        if e.id in AA_table_to_EX():
            disable_import(e)
        elif enable_carbon:
            enable_import(e, rate=1000)
            continue
        elif ["C" in m.formula for m in e.metabolites.keys()][0]:
            disable_import(e)
        else:
            enable_import(e, rate=1000)
    enable_import(model.exchanges.EX_chol_e)
    disable_import(model.exchanges.EX_nh4_e)
    disable_import(model.exchanges.EX_so4_e)
    disable_import(model.exchanges.EX_cysi__L_e)  # this is a dimer of cysteine
    model.reactions.Biomass.bounds = (1.0, 1.0)
    add_feed_supply_reaction(model, feed)
    obj_0 = {model.reactions.get_by_id(feed.name): -1}
    obj_i = obj_0
    model.objective = obj_0
    if pfba:
        sol = cobra.flux_analysis.pfba(model)
    else:
        sol = model.optimize()
    lim = lim_met(model, sol, pfba=pfba, by_mass=lim_by_mass)
    sols = {lim: sol}
    for _ in range(n):
        rxn = model.exchanges.get_by_id(re.sub(r"\d+", "", lim))
        increase_import(rxn, step_size)
        # to tackle the beneficial export: disable export
        disable_export(rxn)
        #
        obj_i[rxn] = (
            aa_cost
            * 0.001
            * model.metabolites.get_by_id(re.sub(r"\d+", "", lim)[3:]).formula_weight
        )
        model.objective = obj_i
        if pfba:
            sol = cobra.flux_analysis.pfba(model)
        else:
            sol = model.optimize()
        lim = lim_met(model, sol, pfba=pfba)
        if lim in sols.keys():
            number = 0
            lim = lim + str(number)
            while lim in sols.keys():
                number += 1
                lim = re.sub(r"\d+", str(number), lim)
        sols[lim] = sol
    model.objective = {model.reactions.get_by_id(feed.name): -1}
    return sols


def add_feed_supply_reaction(model, feed):
    """
    Define reaction with provided AA composition.

        Parameters:
            model (cobra.core.Model): metabolic model
            feed (pd.Series): Amino acid composition
    """
    rxn = cobra.Reaction(id=feed.name, name=feed.name)
    model.add_reactions([rxn])
    rxn.bounds = (0.0, 1000.0)
    for aa, val in feed.iteritems():
        if "/" in aa:
            aa_list = aa.split("/")
        else:
            aa_list = [
                aa,
            ]
        for a in aa_list:
            a = AA_isomer(a) + "_e"
            rxn.add_metabolites({a: gram_to_mol(model, a, val)})


def feed_efficiency(sols_list, model):
    """
    Preprocessing for plotting functions below

        Parameters:
            sols_list (list): list of cobra.core.solution
            model (cobra.core.Model): metabolic model

        Returns:
            feed_cons (pd.DataFrame): The consumed amount of feed
            surplus (pd.DataFrame):
            supplements (pd.DataFrame):
    """
    aa_feed = dict()
    for o in model.objective.variables:
        name = o.name
        if "reverse" in name:
            continue
        else:
            objective = name

    for lim, sol in sols_list.items():
        aa_dict = {"aa": list(), "feed": list(), "surplus": list()}
        for met, val in model.reactions.get_by_id(objective).metabolites.items():
            aa_dict["aa"].append(met.id)
            met_weight = model.metabolites.get_by_id(met.id).formula_weight
            aa_dict["feed"].append(val * sol.fluxes[objective] * met_weight)
            aa_dict["surplus"].append(sol["EX_" + met.id] * met_weight)
        aa_feed[lim] = pd.DataFrame(aa_dict).set_index("aa")
    feed_cons = pd.DataFrame({lim: df.feed for lim, df in aa_feed.items()})
    surplus = pd.DataFrame({lim: df.surplus for lim, df in aa_feed.items()})
    supplements = surplus[[lim for lim in aa_feed.keys()]]
    surplus = surplus[surplus > 0].fillna(0)
    supplements = (
        supplements[supplements < 0]
        .fillna(0)
        .abs()
        .loc[
            list(dict.fromkeys([re.sub(r"\d+", "", lim[3:]) for lim in aa_feed.keys()]))
        ]
    )
    return feed_cons, surplus, supplements


def plot_feed_efficiency(sols_list, model, ax=False, title=""):
    """
    Plot the feed efficiency suplus of amino acids and amino acid supplements

        Parameters:
            sols_list (list): list of cobra.core.solution
            model (cobra.core.Model): metabolic model
            ax (tuple): ax if part of subplot
    """
    if ax:
        pass
    else:
        _, ax = plt.subplots()
    feed_cons, _, supplements = feed_efficiency(sols_list=sols_list, model=model)
    supt = supplements.T
    supt["feed\n  protein"] = feed_cons.sum()
    labels = list(supt.columns)
    labels = [lab.replace("__L", "").replace("_e", "") for lab in labels]
    labels = ["  feed\n  protein"] + [("+ " + lab) for lab in labels]
    supt.plot.bar(
        ax=ax, stacked=True, width=0.5, color=col_hex[[lab[2:] for lab in labels][1:]]
    )
    ax.set_xticks(range(len(labels) - 1))
    ax.set_xticklabels([])
    ax.xaxis.set_minor_locator(MultipleLocator(0.5))
    ax.tick_params(which="minor", labelsize=18, pad=10)
    ax.set_xticklabels([""] + labels[:-2], rotation="horizontal", minor=True)
    _, labels = ax.get_legend_handles_labels()
    cmap = {
        label.split("_")[0]: handle.get_facecolor()
        for handle, label in zip(ax.legend().legendHandles, labels)
    }
    [
        aa.set_color(cmap[aa.get_text()[2:]])
        for aa in ax.xaxis.get_ticklabels(minor=True)[1:-1]
        if len(aa.get_text())
    ]
    ax.tick_params(axis="x", labelrotation=90)
    ax.tick_params(axis="y", which="major", labelsize=18)
    ax.set_ylabel("mg amino acid source\nper g biomass produced", fontsize=20)
    ax.set_xlabel(r"Amino acid supplements $\rightarrow$", fontsize=20)
    ax.set_title(title, y=0.83, fontsize=28)
    ax.get_legend().remove()
